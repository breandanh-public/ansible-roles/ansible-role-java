# Java

Installs Java (default version 12).

## Requirements

None.

## Role Variables

* __java_version__: The version of Java to install \[Default `12`\]

## Dependencies

None.

## Example Playbook

```
- hosts: servers
  roles:
     - role: java
       vars:
         java_version: 12
```


## License

Apache 2.0

## Author Information

BreandanH
