export JAVA_HOME=/usr/lib/jvm/java-{{ java_version }}-oracle
export DERBY_HOME=$JAVA_HOME/db
export J2SDKDIR=$JAVA_HOME
export J2REDIR=$JAVA_HOME
export PATH=$PATH:$JAVA_HOME/bin:$DERBY_HOME/bin
